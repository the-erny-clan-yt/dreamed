
package net.mcreator.curseddreamjar.potion;

import net.minecraft.world.entity.ai.attributes.AttributeMap;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.effect.MobEffect;

import net.mcreator.curseddreamjar.procedures.DreamedEffectExpiresProcedure;

public class DreamedMobEffect extends MobEffect {
	public DreamedMobEffect() {
		super(MobEffectCategory.HARMFUL, -10027264);
	}

	@Override
	public String getDescriptionId() {
		return "effect.cursed_dreamjar.dreamed";
	}

	@Override
	public void removeAttributeModifiers(LivingEntity entity, AttributeMap attributeMap, int amplifier) {
		super.removeAttributeModifiers(entity, attributeMap, amplifier);
		DreamedEffectExpiresProcedure.execute(entity.level(), entity.getX(), entity.getY(), entity.getZ(), entity);
	}

	@Override
	public boolean isDurationEffectTick(int duration, int amplifier) {
		return true;
	}
}
