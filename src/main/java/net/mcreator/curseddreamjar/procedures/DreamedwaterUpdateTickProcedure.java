package net.mcreator.curseddreamjar.procedures;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.core.BlockPos;

import net.mcreator.curseddreamjar.init.CursedDreamjarModBlocks;

public class DreamedwaterUpdateTickProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z) {
		if (Math.random() < 0.1) {
			if ((world.getBlockState(BlockPos.containing(x + 0, y + 1, z + 0))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x + 0, y + 1, z + 0))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x + 0, y + 1, z + 0), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else if ((world.getBlockState(BlockPos.containing(x + 0, y - 1, z + 0))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x + 0, y - 1, z + 0))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x + 0, y - 1, z - 0), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else if ((world.getBlockState(BlockPos.containing(x + 1, y + 0, z + 0))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x + 1, y + 0, z + 0))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x + 1, y + 0, z + 0), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else if ((world.getBlockState(BlockPos.containing(x - 1, y + 0, z + 0))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x - 1, y + 0, z + 0))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x - 1, y + 0, z + 0), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else if ((world.getBlockState(BlockPos.containing(x + 0, y + 0, z + 1))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x + 0, y + 0, z + 1))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x + 0, y + 0, z + 1), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else if ((world.getBlockState(BlockPos.containing(x + 0, y + 0, z - 1))).getBlock() == Blocks.WATER || (world.getBlockState(BlockPos.containing(x + 0, y + 0, z - 1))).getBlock() == Blocks.WATER) {
				world.setBlock(BlockPos.containing(x + 0, y + 0, z - 1), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			} else {
				world.setBlock(BlockPos.containing(x + 0, y + 0, z + 0), CursedDreamjarModBlocks.DREAMEDWATER.get().defaultBlockState(), 3);
			}
		}
	}
}
