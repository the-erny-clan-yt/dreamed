
package net.mcreator.curseddreamjar.fluid;

import net.minecraftforge.fluids.ForgeFlowingFluid;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;

import net.mcreator.curseddreamjar.init.CursedDreamjarModItems;
import net.mcreator.curseddreamjar.init.CursedDreamjarModFluids;
import net.mcreator.curseddreamjar.init.CursedDreamjarModFluidTypes;
import net.mcreator.curseddreamjar.init.CursedDreamjarModBlocks;

public abstract class DreamedwaterFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(() -> CursedDreamjarModFluidTypes.DREAMEDWATER_TYPE.get(), () -> CursedDreamjarModFluids.DREAMEDWATER.get(),
			() -> CursedDreamjarModFluids.FLOWING_DREAMEDWATER.get()).explosionResistance(100f).bucket(() -> CursedDreamjarModItems.DREAMEDWATER_BUCKET.get()).block(() -> (LiquidBlock) CursedDreamjarModBlocks.DREAMEDWATER.get());

	private DreamedwaterFluid() {
		super(PROPERTIES);
	}

	public static class Source extends DreamedwaterFluid {
		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends DreamedwaterFluid {
		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
