
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.ForgeSpawnEggItem;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BlockItem;

import net.mcreator.curseddreamjar.item.DreamedwaterItem;
import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, CursedDreamjarMod.MODID);
	public static final RegistryObject<Item> CURSEDDREAM_SPAWN_EGG = REGISTRY.register("curseddream_spawn_egg", () -> new ForgeSpawnEggItem(CursedDreamjarModEntities.CURSEDDREAM, -10027264, -16777216, new Item.Properties()));
	public static final RegistryObject<Item> DREAMBLOCK = block(CursedDreamjarModBlocks.DREAMBLOCK);
	public static final RegistryObject<Item> DREAMEDWATER_BUCKET = REGISTRY.register("dreamedwater_bucket", () -> new DreamedwaterItem());

	private static RegistryObject<Item> block(RegistryObject<Block> block) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()));
	}
}
