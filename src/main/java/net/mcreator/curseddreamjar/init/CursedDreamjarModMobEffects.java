
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.effect.MobEffect;

import net.mcreator.curseddreamjar.potion.DreamedMobEffect;
import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModMobEffects {
	public static final DeferredRegister<MobEffect> REGISTRY = DeferredRegister.create(ForgeRegistries.MOB_EFFECTS, CursedDreamjarMod.MODID);
	public static final RegistryObject<MobEffect> DREAMED = REGISTRY.register("dreamed", () -> new DreamedMobEffect());
}
