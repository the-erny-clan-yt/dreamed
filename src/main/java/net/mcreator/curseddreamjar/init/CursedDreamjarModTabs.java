
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.network.chat.Component;
import net.minecraft.core.registries.Registries;

import net.mcreator.curseddreamjar.CursedDreamjarMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CursedDreamjarModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, CursedDreamjarMod.MODID);
	public static final RegistryObject<CreativeModeTab> DREAM = REGISTRY.register("dream",
			() -> CreativeModeTab.builder().title(Component.translatable("item_group.cursed_dreamjar.dream")).icon(() -> new ItemStack(CursedDreamjarModBlocks.DREAMBLOCK.get())).displayItems((parameters, tabData) -> {
				tabData.accept(CursedDreamjarModBlocks.DREAMBLOCK.get().asItem());
				tabData.accept(CursedDreamjarModItems.DREAMEDWATER_BUCKET.get());
			}).withSearchBar().build());

	@SubscribeEvent
	public static void buildTabContentsVanilla(BuildCreativeModeTabContentsEvent tabData) {

		if (tabData.getTabKey() == CreativeModeTabs.SPAWN_EGGS) {
			tabData.accept(CursedDreamjarModItems.CURSEDDREAM_SPAWN_EGG.get());
		}
	}
}
