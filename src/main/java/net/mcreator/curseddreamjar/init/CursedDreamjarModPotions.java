
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.effect.MobEffectInstance;

import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModPotions {
	public static final DeferredRegister<Potion> REGISTRY = DeferredRegister.create(ForgeRegistries.POTIONS, CursedDreamjarMod.MODID);
	public static final RegistryObject<Potion> DREAMING = REGISTRY.register("dreaming", () -> new Potion(new MobEffectInstance(CursedDreamjarModMobEffects.DREAMED.get(), 3, 0, false, true)));
}
