
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.mcreator.curseddreamjar.block.DreamedwaterBlock;
import net.mcreator.curseddreamjar.block.DreamblockBlock;
import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, CursedDreamjarMod.MODID);
	public static final RegistryObject<Block> DREAMBLOCK = REGISTRY.register("dreamblock", () -> new DreamblockBlock());
	public static final RegistryObject<Block> DREAMEDWATER = REGISTRY.register("dreamedwater", () -> new DreamedwaterBlock());
}
