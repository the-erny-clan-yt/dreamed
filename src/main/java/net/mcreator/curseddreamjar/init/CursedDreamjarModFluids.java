
/*
 * MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ItemBlockRenderTypes;

import net.mcreator.curseddreamjar.fluid.DreamedwaterFluid;
import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModFluids {
	public static final DeferredRegister<Fluid> REGISTRY = DeferredRegister.create(ForgeRegistries.FLUIDS, CursedDreamjarMod.MODID);
	public static final RegistryObject<FlowingFluid> DREAMEDWATER = REGISTRY.register("dreamedwater", () -> new DreamedwaterFluid.Source());
	public static final RegistryObject<FlowingFluid> FLOWING_DREAMEDWATER = REGISTRY.register("flowing_dreamedwater", () -> new DreamedwaterFluid.Flowing());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			ItemBlockRenderTypes.setRenderLayer(DREAMEDWATER.get(), RenderType.translucent());
			ItemBlockRenderTypes.setRenderLayer(FLOWING_DREAMEDWATER.get(), RenderType.translucent());
		}
	}
}
