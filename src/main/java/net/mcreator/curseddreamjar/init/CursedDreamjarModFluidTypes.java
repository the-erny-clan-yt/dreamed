
/*
 * MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.curseddreamjar.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fluids.FluidType;

import net.mcreator.curseddreamjar.fluid.types.DreamedwaterFluidType;
import net.mcreator.curseddreamjar.CursedDreamjarMod;

public class CursedDreamjarModFluidTypes {
	public static final DeferredRegister<FluidType> REGISTRY = DeferredRegister.create(ForgeRegistries.Keys.FLUID_TYPES, CursedDreamjarMod.MODID);
	public static final RegistryObject<FluidType> DREAMEDWATER_TYPE = REGISTRY.register("dreamedwater", () -> new DreamedwaterFluidType());
}
